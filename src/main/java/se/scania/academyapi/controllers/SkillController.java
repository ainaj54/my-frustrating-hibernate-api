package se.scania.academyapi.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.scania.academyapi.models.Skill;
import se.scania.academyapi.models.Student;
import se.scania.academyapi.repositories.SkillRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@Tag(name="Skill", description = "Skill-object with contains id, skill-description and lists teacher possessing skill.")
@RequestMapping("api/academy/skills")
public class SkillController {
    @Autowired
    private SkillRepository skillRepository;

    @Operation(description= "Returns all skills in database")
    @GetMapping()
    public ResponseEntity<List<Skill>> getAllSkills(){
        List<Skill> allSkills = skillRepository.findAll();
        return new ResponseEntity<List<Skill>>(allSkills, HttpStatus.OK);

    }

    @Operation(description = "Returns skill matching id given in path. If no such object exists BAD_REQUEST is returned.")
    @GetMapping("/{id}")
    public ResponseEntity<Skill> getSkill(@PathVariable Long id){

        if(skillRepository.existsById(id)){
            return new ResponseEntity<Skill>(skillRepository.findById(id).get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

    }

    @Operation(description = "Adds skill to database.")
    @PostMapping
    public ResponseEntity<Skill> addSkill(@RequestBody Skill skill){
        Skill s = skillRepository.save(skill);
        return new ResponseEntity<Skill>(s, HttpStatus.OK);
    }

    @Operation (description = "update existing skill-object with id corresponding to given in path. If no such object exists, BAD_REQUEST is returned. ")
    @PutMapping("/{id}")
    public ResponseEntity<Skill> updateSkill(@PathVariable Long id, @RequestBody Skill skill){
        Skill newSkill = new Skill();

        if(id.equals(skill.getId())){
            newSkill = skillRepository.save(skill);
            return new ResponseEntity<Skill>(newSkill, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @Operation(description = "deletes skill-object with id corresponding to given in path. If no such object exists, BAD_REQUEST is returned.")
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteSkill(@PathVariable Long id){
        if(skillRepository.existsById(id)){
            skillRepository.deleteById(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }



}
