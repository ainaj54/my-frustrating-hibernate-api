package se.scania.academyapi.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.scania.academyapi.models.Student;
import se.scania.academyapi.models.Teacher;
import se.scania.academyapi.repositories.TeacherRepository;

import java.util.List;
@Tag(name="Teacher", description = "Teacher object and endpoints")
@RestController
@RequestMapping("api/academy/teachers")
public class TeacherController {
    @Autowired
    TeacherRepository teacherRepository;

    @Operation(description = "Get all teacher objects in database. ")
    @GetMapping
    public ResponseEntity<List<Teacher>> getAllTeachers(){
        return new ResponseEntity<List<Teacher>>(teacherRepository.findAll(), HttpStatus.OK);
    }

    @Operation(description = "Get teacher connected to id given as path variable. ")
    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getTeacher(@PathVariable Long id){
        if(teacherRepository.existsById(id)){
            Teacher teacher = teacherRepository.findById(id).get();
            return new ResponseEntity<Teacher>(teacher, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @Operation(description = "Add new teacher object. No fields are forced. Id automatically added.")
    @PostMapping
    public ResponseEntity<Teacher> addTeacher(@RequestBody Teacher teacher){
        Teacher newTeacher = teacherRepository.save(teacher);
        return new ResponseEntity<Teacher>(newTeacher, HttpStatus.OK);
    }

    @Operation(description = "Updates Teacher-object in database with corresponding id given in path. If no such objet exist BAD_REQUEST is returned. ")
    @PutMapping("/{id}")
    public ResponseEntity<Teacher> updateTeacher(@PathVariable Long id, @RequestBody Teacher teacher){
        Teacher newTeacher = new Teacher();

        if(id.equals(teacher.getId())){
            newTeacher = teacherRepository.save(teacher);
            return new ResponseEntity<Teacher>(newTeacher, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @Operation(description = "Deletes teacher-object with corresponding id given in path. If no such object exists BAD_REQUEST is returned.")
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteTeacher(@PathVariable Long id){
        if(teacherRepository.existsById(id)){
            teacherRepository.deleteById(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }


}
