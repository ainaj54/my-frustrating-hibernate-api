package se.scania.academyapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.scania.academyapi.models.Student;
import se.scania.academyapi.models.Teacher;
import se.scania.academyapi.repositories.StudentRepository;

import java.util.List;

@RestController
@Tag(name = "Student", description = "Object containing student id, name and lists student's teachers. ")
@RequestMapping("/api/academy/students")
public class StudentController {

    @Autowired
    StudentRepository studentRepository;

    @Operation(description = "Returns all student-object in database")
    @GetMapping
    public ResponseEntity<List<Student>> getAllStudents(){
        List<Student> students = studentRepository.findAll();
        return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
    }

    @Operation(description = "Returns specific student with corresponding id given in path. If no such object exists NOT_FOUND is returned.")
    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudent(@PathVariable Long id){
        if(studentRepository.existsById(id)){
            Student student = studentRepository.findById(id).get();
            return new ResponseEntity<Student>(student, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }


    @Operation(description = "Adds student-object to database. No fields are enforced, id is added automatically.")
    @PostMapping
    public ResponseEntity<Student> addStudent(@RequestBody Student student){
        Student s = studentRepository.save(student);
        return new ResponseEntity<Student>(s, HttpStatus.OK);
    }

    @Operation(description = "Updates Student-object with corresponding id given in path. If no such object exists BAD_REQUST is returned. ")
    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody Student student){
        Student newStudent = new Student();

        if(id.equals(student.getId())){
            newStudent = studentRepository.save(student);
            return new ResponseEntity<Student>(newStudent, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @Operation(description = "Deletes student-object with id corresponding to path-variable. If no such student-object exists, BAD_REQUEST is returned.")
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteStudent(@PathVariable Long id){
        if(studentRepository.existsById(id)){
            studentRepository.deleteById(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

}
