package se.scania.academyapi.models;
import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;


import static javax.persistence.GenerationType.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Teacher {


    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email")
    private String email;
    @Column(name = "phone")
    private String phone;

    @ManyToMany
    @JoinTable(name = "Skill_Teacher",
            joinColumns = {@JoinColumn(name= "teacher_id")},
            inverseJoinColumns = {@JoinColumn(name= "skill_id")})
    public List<Skill> skills;

    @JsonGetter("skills")
    public List<String> skillsGetter(){
        if(skills != null) {
            return skills.stream().map(skill -> {
                return "api/academy/skills/" + skill.getId();
            }).collect(Collectors.toList());
        }
        return null;
    }



    @OneToMany
    @JoinColumn(name="student_teacher" )
    private List<Student> students;
    @JsonGetter("students")
    public List<String> students(){
        if(students != null) {
            return students.stream()
                    .map(student -> {
                        return "/api/academy/students/" + student.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }



    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long teacherId) {
        this.id = teacherId;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


}



