package se.scania.academyapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Skill {
        @Id
        @GeneratedValue(strategy = IDENTITY)
        private Long id;
        @Column(name= "skill")
        private String skill;
        @ManyToMany
        @JoinTable(name = "Skill_Teacher",
                joinColumns = {@JoinColumn(name= "skill_id")},
                inverseJoinColumns = {@JoinColumn(name= "teacher_id")})
        public List<Teacher> teachers;
        @JsonGetter("teachers")
        public List<String> teachers(){
            if(teachers != null) {
                return teachers.stream().map(teacher -> {
                    return "api/academy/teachers/" + teacher.getId();
                }).collect(Collectors.toList());
            }
            return null;
        }

        public String getSkill() {
            return skill;
        }

        public void setSkill(String skill) {
            this.skill = skill;
        }



    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}


