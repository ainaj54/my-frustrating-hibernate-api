package se.scania.academyapi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static java.awt.SystemColor.info;

@SpringBootApplication
@OpenAPIDefinition(

)
public class AcademyApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AcademyApiApplication.class, args);
    }

}
