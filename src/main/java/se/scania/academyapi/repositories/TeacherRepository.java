package se.scania.academyapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.scania.academyapi.models.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
