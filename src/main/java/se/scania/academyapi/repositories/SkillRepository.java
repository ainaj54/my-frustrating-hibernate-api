package se.scania.academyapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.scania.academyapi.models.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {
}
