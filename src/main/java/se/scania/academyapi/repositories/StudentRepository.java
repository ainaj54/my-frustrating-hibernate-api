package se.scania.academyapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.scania.academyapi.models.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
